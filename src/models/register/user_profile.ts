import { UUID } from "crypto";
import { Knex } from "knex";

export class UserProfile {

    //CREATE BY RENFIX list รายการ user ออกมา
    info(db: Knex, userId: UUID) {
        console.log(userId);
        
        return db('user').select('profile.*','user.roles','user.last_login_at','provider.name as provider_name')
            .innerJoin('profile','profile.user_id','user.id')
            .leftJoin('provider','provider.id','profile.provider_id')
            .where('user.id', userId)
            // .andWhere('provider.council_code','01')
    }



}