import S from 'fluent-json-schema'

//Create by RENFIX เช็ค ว่า userId ที่เข้ามาเป็น uuid ไหม
const paramsSchema = S.object()
  .prop('user_id', S.string().format('uuid').required())
  

export default {
    params: paramsSchema
}