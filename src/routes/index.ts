import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify";
import { StatusCodes } from 'http-status-codes';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  fastify.get('/',async (request: FastifyRequest, reply: FastifyReply) => {
      return reply.status(StatusCodes.OK).send({ ok: true , message: 'IPD Paperless  --> Auth Services, RESTful API services! 2024R10'});
  })

  fastify.register(require('./login'), { prefix: '/auth' });

  done();

} 
